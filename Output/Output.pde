/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;
import gab.opencv.*;
import processing.sound.*;

// to get Java Rectangle type
import java.awt.*; 

Capture cam;
OpenCV opencv;

// scale factor to downsample frame for processing 
float scale;

// image to display
PImage output;
PImage background;
PImage before;
PImage changing;
PImage faceImages[];

PImage faceArea;
PImage faceMask;

Boolean firstTime;
int nfaces;
int face_track;
int face_track_auto;
Boolean click;
float interval;
float lastTime;
float lastTime_auto;
Boolean auto;

SoundFile music;
// array of bounding boxes for face
Rectangle[] faces;


void setup() {
  size(1680, 1000);
  scale = 0.5;
  // fullScreen();

  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale));

  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  

  cam.start();

  // init to empty image
  output = new PImage(cam.width, cam.height);

  // faceArea = loadImage("1.png");
  nfaces = 10;
  faceImages = new PImage[nfaces];
  face_track = 0;

  for (int i=0; i<nfaces; i+=1) {
    String s = i+".png";
    faceImages[i] = loadImage(s);
  }

  background = loadImage("normal.png");
  background.resize(1680, 1000);
  faceMask = loadImage("mask.png");
  faceMask.filter(GRAY);
  faceMask.filter(INVERT);

  before = loadImage("face_emoji.png");
  before.resize(1680, 1000);
  changing = loadImage("face_changing.png");
  changing.resize(1680, 1000);

  firstTime = true;
  interval = 1000;
  auto = false;
  face_track_auto = 0;

  music = new SoundFile(this, "sample.mp3");
  music.play();
}


void draw() {
  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);

    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);

    faces = opencv.detect();

    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot();
  }

  // draw the image
  pushMatrix();
  // scale the image back up for display 
  // (this is the inverse of the downsample scale)
  scale(1 / scale); 
  // image(background, 0, 0);
  popMatrix();

  // draw face tracking debug
  if (faces != null) {
    if (faces.length == 0) {
      background(changing); 
      firstTime = false;
      if (click == true) {
        click = false;
        interval = millis()-lastTime;
        lastTime = millis();
        face_track += 1;
        if (face_track > nfaces) {
          face_track = 0;
        }
        if (interval < 500) {
          auto = true;
        } else {
          auto = false;
        }
      }
    } else {
      background(background);
      if (auto == false) {
        click = true;
        for (int i = 0; i < faces.length; i++) {
          // scale the tracked faces to canvas size
          float s = 1 / scale;
          int x = int(faces[i].x * s);
          int y = int(faces[i].y * s);
          int w = int(faces[i].width * s);
          int h = int(faces[i].height * s);

          /*
         draw bounding box and a "face id"
           stroke(255, 255, 0);
           noFill();     
           rect(x, y, w, h);
           fill(255, 255, 0);
           text(i, x, y - 20);
           */

          if (firstTime) {
            faceArea = output.get(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
            faceArea.resize(180, 180);
            faceMask.resize(180, 180);
            faceArea.mask(faceMask);

            image(faceArea, 690, 285);
          } else {
            background(before);
            if (face_track < nfaces) {
              faceImages[face_track].resize(180, 180);
              image(faceImages[face_track], 725, 230);
            } else {
              faceArea = output.get(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
              faceArea.resize(180, 180);
              faceMask.resize(180, 180);
              faceArea.mask(faceMask);

              image(faceArea, 715, 270);
            }
          }
        }
      }
      else {
        click = true;
        background(before);
        if (face_track_auto >= 10) {
            face_track_auto = 0;
        }
        if (millis()-lastTime_auto < 3) {
          faceImages[face_track_auto].resize(180, 180);
          image(faceImages[face_track_auto], 725, 230);
        } else {
          faceImages[face_track_auto].resize(180, 180);
          image(faceImages[face_track_auto], 725, 230);
          face_track_auto += 1;
          lastTime_auto = millis();
        }
      }
    }
  }

  fill(255, 0, 0);
  // nfc is a function to format numbers, second argument is 
  // number of decimal places to show
  text(nfc(frameRate, 1), 20, 20);
}
