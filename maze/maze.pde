PShape outerMaze;
float outerXangle;
float outerYangle;

PShape innerMaze;
int mode;
float innerXangle;
float innerYangle;

PVector[][] centerShape;
int totalVertices;

Boolean flash;
float flashTime;
int offset;

// super shape constants
float m = 0;
float mchange = 0;
float a = 1;
float b = 1;

float supershape(float theta, float m, float n1, float n2, float n3) {
  float t1 = abs((1/a)*cos(m * theta/4));
  t1 = pow(t1, n2);
  float t2 = abs((1/b)*sin(m * theta/4));
  t2 = pow(t2, n3);
  float t3 = t1 + t2;
  float r = pow(t3, -1/n1);
  
  return r;
}

// rotation modes for inner maze
void randomRotate(int mode) {
  switch (mode) {
    case 1:
      innerXangle = 0.001;
      innerYangle = 0.001;
      break;
    case 2:
      innerXangle = 0.010;
      innerYangle = 0.005;
      break;
    case 3:
      innerXangle = 0.007;
      innerYangle = 0.012;    
      break;
    case 4:
      innerXangle = 0.004;
      innerYangle = 0.006;
      break;
    case 5: 
      innerXangle = 0.002;
      innerYangle = 0.005;
      break;
  }
}

void setup() {
  size(1200, 1000, P3D);
  
  outerMaze = loadShape("maze.obj");
  outerMaze.setFill(color(255));
  outerMaze.scale(8);
  outerXangle = 0.002;
  outerYangle = 0.005;
  
  innerMaze = loadShape("maze.obj");
  innerMaze.scale(2);
  innerMaze.setFill(color(255));
  mode = 5;
  
  totalVertices = 75;
  centerShape = new PVector[totalVertices+1][totalVertices+1];
  
  flashTime = 0;
  flash = false;
}

void draw() {
  background(0);
  lights();
  noStroke();
  
  // check the flashing time lasting
  if (flash && millis()-flashTime>2000) {
      flash = false;
  }

  // move 0, 0, 0 to centre of canvas
  translate(width/2, height/2, -200);
  
  // draw 3D mazes 
  outerMaze.rotateY(outerYangle);
  outerMaze.rotateX(outerXangle);
  shape(outerMaze);
  
  randomRotate(mode);
  innerMaze.rotateX(innerXangle);
  innerMaze.rotateY(innerYangle);
  shape(innerMaze);
  
  // draw center super shape
  m = map(sin(mchange), -1, 1, 0, 7);
  mchange += 0.02;
  float r = 20;
  
    // calculate the coordinates of each vertices
  for (int i = 0; i <= totalVertices; i++) {
    float lat = map(i, 0, totalVertices, -HALF_PI, HALF_PI);
    float r2 = supershape(lat, m, 0.2, 1.7, 1.7);
    for (int j = 0; j <= totalVertices; j++) {
      float lon = map(j, 0, totalVertices, -PI, PI);
      float r1 = supershape(lon, m, 0.2, 1.7, 1.7);
      float x = r * r1 * cos(lon) * r2 * cos(lat);
      float y = r * r1 * sin(lon) * r2 * cos(lat);
      float z = r * r2 * sin(lat);
      centerShape[i][j] = new PVector(x, y, z);
    }
  }
  
  offset += 5;
    // create the super shape with the coordinates
  for (int i = 0; i < totalVertices; i++) {
    if (flash) {
      float hu = map(i, 0, totalVertices, 0, 255*6);
      float c = (hu + offset) % 255;
      fill(c , c, c);
    } else {
      fill(255, 255, 255);
    }
    beginShape(TRIANGLE_STRIP);
    for (int j = 0; j <= totalVertices; j++) {
      PVector v1 = centerShape[i][j];
      PVector v2 = centerShape[i+1][j];
      vertex(v1.x, v1.y, v1.z);
      vertex(v2.x, v2.y, v2.z);
    }
    endShape();
  }
} 


// receive user inputs to manipulate the action of inner maze
void keyPressed() {
  flash = true;
  flashTime = millis();
  switch (keyCode) {
    case LEFT: 
      if (mode >= 5) {
        mode = 0;
      } else {
        mode ++;
      }
      break;
    case RIGHT: 
      if (mode <= 0) {
        mode = 5;
      } else {
        mode --;
      }
      break;
    default: 
      print("here");
      break;
  }
} 
