import processing.sound.*;

SoundFile[] file;
int numsounds = 5;
int[] playSound = {0,0,0,0,0};
int trigger;
int done = 0;

// global variables for agents
float xfactor = 1;
float yfactor = 1;
int drawMode = 8;
int tiles = 10; // grid size is tiles by tiles
PImage img;

// list of agents
ArrayList<Agent> agents;
ArrayList<Integer> index;
SoundFile sigh;

void setup() {
  //size should be multiple of img width and height
  size(1000, 1000); 
  // fullScreen();
  
  // create sounds files
  file = new SoundFile[numsounds];
  for (int i=0; i<numsounds; i++){
    // file[i] = new SoundFile(this, (i+1)+".mp3");
    file[i] = new SoundFile(this, "1.mp3");
  }
  trigger = millis(); 
  sigh = new SoundFile(this, "sigh.wav");
  
  // visualization part
  img = loadImage("me.png");
  createAgents();
  
  index = new ArrayList<Integer>();
  for (int i=0; i<agents.size(); i++) {
    index.add(i);
  }
}

// create the grid of agents, one agent per grid location
void createAgents() {

  agents = new ArrayList<Agent>();

  // step size between grid centres
  float step = width / img.width;
  // the length of the agents line (diagonal line of tile)
  
  // create an Agent and place it at centre of each tile
  for (int px = 0; px < img.width; px++) {
    for (int py = 0; py < img.height; py++) {
      Agent a = new Agent(px * step, py * step, px, py);
      agents.add(a);
    }
  }
}

void draw() {
  background(255);
  
  if (millis() > trigger && done == 0){
    int select = int(random(0, 5));
    file[select].play(1.0, 1.0); //<>//
    
    select += 1;
    
    if (millis() < 10000) {
      trigger = millis() + int(random(2000, 2000 * map(mouseX, 0, width, 0, 1)));
    } else if (millis() < 20000){
      trigger = millis() + int(random(1000, 2000 * map(mouseX, 0, width, 0, 1)));
    } else {
      trigger = millis() + int(random(750, 2000 * map(mouseX, 0, width, 0, 1)));
    }

    if (millis() > 5000) {
      select *= 10;
    } else if (millis() > 15000) {
      select *= 1000;
    } else if (millis() > 25000) {
      select *= 10000;
    }
    
    for (int i=0; i<select; i++) {
      if (index.size() == 0) {
        done = 1;
        sigh.play();
        break;
      }
      
      int selected_index = int(random(0, index.size()-1));
      int selected_pixel = index.get(selected_index);
      agents.get(selected_pixel).light = 1;
      
      index.remove(selected_index);
    }
  }

  // draw all the agents
  for (Agent a : agents) { //<>//
    a.update();
    a.draw();
  }
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  
}
