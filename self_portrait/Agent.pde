class Agent {

  // local shape transforms

  // location of agent centre
  int x;
  int y;
  
  // pixel location in image
  int px;
  int py;
  
  int light;
  
  float greyscale;
  color pixelColour;
  PImage icon = loadImage("facebook_small.png");
  
  
  // create the agent
  Agent(float _x, float _y, int _px, int _py) {
    x = (int)_x;
    y = (int)_y;
    px = _px;
    py = _py;
    light = 0;
  }

  void update() {

    // get current color
    pixelColour = img.pixels[py * img.width + px];
    // greyscale conversion
    greyscale = round(red(pixelColour)*0.222+green(pixelColour)*0.707+blue(pixelColour)*0.071);
    //println(x, y, px, py, greyscale);
  }

  void draw() {
    if (light == 1) {
      tint(255, 275-greyscale);
      image(icon, x, y, 16, 16);
    }
  }
}
